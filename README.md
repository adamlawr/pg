# pg

alpine based docker container for postgres database server. includes scripts for backing and restoring with aws s3.

## normal useage
from the deploy script of an app or in an ssh session on the server (probably an ec2 instance)
pull the image and start a container

    $ docker run -d -p 5432:5432 --env-file .env --name pg registry.gitlab.com/adamlawr/pg`

create a database

    $ docker exec pg createdb -U postgres <db_name>

start the cron daemon for backups

    $ docker exec pg /usr/sbin/crond -d 0 -L /tmp/cronlog

restore the db from a backup on s3

    $ docker exec --env-file .env pg /scripts/db

get in and look around:

    docker exec -it pg /bin/bash

## development

### links
[add ruby to an alpine container](https://blog.codeship.com/build-minimal-docker-container-ruby-apps/)
[postgres 11 manual](https://www.postgresql.org/docs/11/index.html)
[psql manual](https://www.postgresql.org/docs/11/app-psql.html)
[pasword generator](https://passwordsgenerator.net/) - for the .env file

### s3
s3 access with `raceweb-s3` or `raceweb-s3-app` iam user

build:
```
docker rm -f pg
docker image rm moops/pg:rb
docker build -t moops/pg:rb .
docker run -d -p 5433:5432 -v $(pwd)/scripts:/scripts -e AWS_ACCESS_KEY_ID=... -e AWS_SECRET_ACCESS_KEY=... -e DB_NAME=... -e BUCKET=... --name pg moops/pg:rb
```

### create a test database
like this to iterate on a sql script

    $ psql -h localhost -U postgres
    # \i test_db.sql
    # -- repeat if necessary with:
    # \c postgres
    # drop database music;

or like this if you know the sql is in good shape:

    psql -h localhost -U postgres < test_db.sql

### start the cron daemon

    /usr/sbin/crond -d 0 -L /tmp/cronlog

## deploy gitlab container registry

login

    $ docker login registry.gitlab.com

build and push image to gitlab

    $ docker build -t registry.gitlab.com/adamlawr/pg:20200629 .
    $ docker push registry.gitlab.com/adamlawr/pg:20200629
