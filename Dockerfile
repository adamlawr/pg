FROM postgres:13-alpine

LABEL author="adam@raceweb.ca" \
      url="https://gitlab.com/adamlawr/pg"

################################################################################
# install ruby
################################################################################
RUN apk update && apk upgrade && apk add bash curl-dev ruby-dev build-base && \
    apk add ruby ruby-io-console ruby-bundler ruby-webrick && \
  rm -rf /var/cache/apk/*

RUN mkdir /scripts
WORKDIR /scripts
COPY ./scripts .

# set backup script to run daily
# make scripts executable
# install gems
# set timezone
RUN mv /scripts/cron /etc/periodic/daily/backup && \
  chmod +x /scripts/db && \
  chmod +x /etc/periodic/daily/backup && \
  echo 'gem: --no-document' >> /etc/gemrc && \
  gem install bundler -v 1.17.3 && \
  bundle install && \
  cp /usr/share/zoneinfo/America/Vancouver /etc/localtime && \
  echo "America/Vancouver" > /etc/timezone
